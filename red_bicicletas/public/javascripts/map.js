var mymap = L.map('mapid').setView([-27.076254,-55.642662], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a>'
}).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(
            function(bici){
                L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
            }
        );
    }
});    