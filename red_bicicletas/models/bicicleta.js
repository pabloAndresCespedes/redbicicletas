var Bicicleta = function(id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toSting = function (){
    return 'id: ' + this.id + ' | color: ' + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function (aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);

    if(aBici)
        return aBici;
    else
        throw new Error (`No existe una bicicleta con el ID ${aBici}`);
}

Bicicleta.removeById = function (aBiciId){
    //var aBici = Bicicleta.findById(aBiciId);
    for(var i = 0; i < Bicicleta.length; i++){
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}
/*
var a = new Bicicleta(1, 'Rojo', 'urbana', [-27.0838296,-55.6528657]);
var b = new Bicicleta(2, 'Blanca', 'urbana', [-27.0804062,-55.6474953]);

Bicicleta.add(a);
Bicicleta.add(b);*/

module.exports = Bicicleta;